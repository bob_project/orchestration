#!/usr/bin/env python3
import argparse
import asyncio
import logging
import os
import threading
import yaml
import time

from asgiref.sync import async_to_sync
from datetime import datetime, timedelta
from dotenv import load_dotenv

from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.common.exceptions import TimeoutException
from selenium.webdriver.firefox.options import Options

from websocket_client import WebSocketClient, StopException

load_dotenv(verbose=True, override=True, dotenv_path=".env")

# logging configuration
logger = logging.getLogger('client')
handler = logging.StreamHandler()
handler.setFormatter(logging.Formatter('\033[30;1m[%(asctime)s][%(levelname)s] \033[0m%(message)s', datefmt='%d/%m/%Y %H:%M:%S'))
logger.addHandler(handler)

# global variable configuration
BACKEND_DELAY = 5
PING_INTERVAL = 10
MAX_RETRY = 10

class BackendWrapper:
    """
    A simple wrapper to imitate BackendWorker

    Attributes:
        url:           URL to WebSocket server
        delay:         Delay between each loop
        token:         Connection token
        stop:          Stop flag
        ping_interval: Websocket conenction ping interval
        max_retry:     Number of time to retry sending a message
    """

    def __init__(self, url, token, delay=BACKEND_DELAY, ping_interval=PING_INTERVAL, max_retry=MAX_RETRY):
        self.url = url
        self.delay = delay
        self.token = token
        self.ping_interval = ping_interval
        self.max_retry = max_retry
        self.stop = False        

class ComputerClient(WebSocketClient):
    """
    A Websocket client to coordinate computer runs

    Attributes:
        configuration:    Configuration loaded from YAML file
        short_by_thread:  Maximum number of run per short worker
        threads:          Pool of threads

        run_count:        Counter of run
        total_run:        Total amount of run to perform
        count_lock:       Lock for concurrent access on run_count
    """

    def __init__(self, url, token, configuration):

        backend = BackendWrapper(url, token)
        super().__init__(backend)
        self.add_task(self.master_task(self.manual_stop))

        self.configuration = configuration
        self.short_by_thread = max(configuration["saboteur_timeout"], configuration["long_timeout"]) // configuration["short_timeout"]
        self.threads = []

        self.run_count = 0
        self.total_run = 0
        self.count_lock = threading.Lock()
        self.running = False
        
        logger.info("\033[92mCalculated number of short worker run by thread: \033[33m{}\033[0m".format(self.short_by_thread))

    def increment(self):
        """ Count number of run performed on total amount. """

        # concurrent access
        with self.count_lock:
            self.run_count += 1
            logger.info("\033[92mRun pourcentage: \033[33m{}% ({}/{})\033[0m".format(
                round(self.run_count / self.total_run * 100, 2),
                self.run_count, self.total_run
            ))
        async_to_sync(self.send)({"type": "END_RUN"})

    def start_threads(self, message):
        """
        Create all threads with received configuration.

        Parameters:
            message: Received configuration
        """

        # calculate to number of run
        self.run_count = 0
        self.total_run = message["nb_saboteur"] + message["nb_long"] + message["nb_short"]

        # print configuration
        logger.info((
            "\033[92mReceived configuration (total: \033[33m{}\033[92m)\033[0m\n"
            "- \033[33;1mShort\033[0m: \033[33m{}\033[0m\n"
            "- \033[33;1mLong\033[0m: \033[33m{}\033[0m\n"
            "- \033[33;1mSaboteur\033[0m: \033[33m{}\033[0m"
        ).format(self.total_run, message["nb_short"], message["nb_long"], message["nb_saboteur"]))

        # create all threads
        self.threads = []
        self.threads += self.create_threads(int(message["nb_saboteur"]), saboteur=True)
        self.threads += self.create_threads(int(message["nb_short"]), short=True)
        self.threads += self.create_threads(int(message["nb_long"]))

        # start all threads
        for thread in self.threads:
            thread.start()
        logger.info("\033[92mStarted all threads\033[0m")

    def wait_threads(self):
        """ Wait all treads to finish. """

        for thread in self.threads:
            thread.join()
        logger.info("\033[92mFinished all threads\033[0m")
        self.running = False
        async_to_sync(self.send)({"type": "FINISHED_RUN"})

    async def receive(self, message):
        """
        Process message receive from websocket conenction.

        Parameters:
            message: Message received
        """

        # check hasn't to stop
        if self.backend.stop:
            raise StopException()

        # check message type
        if message["type"] == "START_CLIENT" and not self.running:
            logger.info("\033[92mBatch infos: {}\033[0m".format(message["batch"]))
            self.running = True

            self.start_threads(message)
            await self.send({"type": "START_RUN", "total_run": self.total_run})

            threading.Thread(target=self.wait_threads, args=[]).start()
        else:
            logger.error("\033[92mUnkown message type recieved from server: {}\033[0m".format(message["type"]))

    async def manual_stop(self):
        """ Event loop which check for manual stop. """

        # check STOP environnement variable isn't at True
        load_dotenv(verbose=True, override=True, dotenv_path=".env")
        if os.environ.get("STOP"):
            self.backend.stop = os.environ.get("STOP").lower() in ['true', 'yes', '1']
        # if marked to stop, signal to all threads to stop
        if self.backend.stop:
            raise StopException()

    def create_driver(self):
        """
        Create a browser driver.
        """

        # check for headless option
        options = Options()
        if self.configuration["headless"]:
            options.add_argument("--headless")

        # create driver
        if self.configuration["gecko_path"]:
            driver = webdriver.Firefox(options=options, executable_path=self.configuration["gecko_path"])
        else:
            driver = webdriver.Firefox(options=options)
        return driver

    def wait_ban(self, driver, timeout):
        """
        Wait until worker get banned

        Parameters:
            driver: Browser driver
            timeout: Maximum timeout
        """

        WebDriverWait(driver, timeout).until(
            EC.text_to_be_present_in_element((By.ID, "ban"), "(yes)")
        )

    def honest_run(self, nb_run, timeout, index, waiting):
        """
        Create a honest worker

        Parameters:
            nb_run: Number of run to perform by this worker
            timeout: Maximum amount of time for worker to stay alive
            index: Worker number
        """

        time.sleep(waiting)

        # format label
        label = "\033[96mLONG_{}\033[0m".format(index)
        if timeout == self.configuration["short_timeout"]:
            label = "\033[35mSHORT_{}\033[0m".format(index)

        total_time_spent = 0

        # repeat a run for nb_run times
        for run_nb in range(nb_run):
            logger.info("[{}][{}/{}][START]".format(label, run_nb + 1, nb_run))

            # create browser driver
            driver = self.create_driver()
            start = datetime.now()

            # ask browser to request provided url
            logger.debug("[{}][{}/{}][RUNNING] Requesting: {}".format(label, run_nb + 1, nb_run, self.configuration["honest_url"]))
            driver.get(self.configuration["honest_url"])

            try:
                # wait for worker to be banned
                logger.debug("[{}][{}/{}][RUNNING] Waiting ...".format(label, run_nb + 1, nb_run))
                self.wait_ban(driver, timeout)
                logger.info("[{}][{}/{}][RUNNING] Banned".format(label, run_nb + 1, nb_run))
            except TimeoutException:
                # if not ban, reach timeout
                logger.debug("[{}][{}/{}][RUNNING] Timeout, not banned".format(label, run_nb + 1, nb_run))
            finally:
                # calculate time spent on run and increment total time spent
                end = datetime.now()
                time_spent = int(((end - start) - timedelta(microseconds=(end - start).microseconds)).total_seconds())
                total_time_spent += time_spent

                # close browser
                logger.debug("[{}][{}/{}][RUNNING] Quitting driver".format(label, run_nb + 1, nb_run))
                driver.quit()

                # display time spent on run
                if nb_run > 1:
                    logger.info("[{}][{}/{}][END] Spent {}".format(label, run_nb + 1, nb_run, str(timedelta(seconds=time_spent))))
            self.increment()

        # display total time spent
        logger.info("[{}] Total time spent: {}".format(label, str(timedelta(seconds=total_time_spent))))

    def saboteur_run(self, index):
        """
        Create a saboteur worker

        Parameters:
            index: Saboteur number
        """

        # format label wit saboteur type
        label = "\033[31mSABOTEUR_{}\033[0m".format(index)

        total_time_spent = 0
        timeout = self.configuration["saboteur_timeout"]

        # keep runing while not reach maximum timeout
        while timeout > 0:
            logger.info("[{}][START]".format(label))
            if total_time_spent:
                # display time spent on saboteur run before getting ban
                logger.info("[{}] Time spent: {} / {}".format(
                    label,
                    str(timedelta(seconds=total_time_spent)),
                    str(timedelta(seconds=self.configuration["saboteur_timeout"]))
                ))

            # create browser driver
            driver = self.create_driver()
            start = datetime.now()

            # ask browser to request provided url
            logger.debug("[{}] Requesting: {}".format(label, self.configuration["saboteur_url"]))
            driver.get(self.configuration["saboteur_url"])

            try:
                # wait for worker to be banned
                logger.debug("[{}] Waiting ...".format(label))
                self.wait_ban(driver, timeout)
                logger.info("[{}] Banned".format(label))
            except TimeoutException:
                # if not ban, reach timeout
                logger.debug("[{}] Timeout, not banned".format(label))
            finally:
                # calculate time spent on run and increment total time spent
                end = datetime.now()
                time_spent = int(((end - start) - timedelta(microseconds=(end - start).microseconds)).total_seconds())
                total_time_spent += time_spent
                timeout -= time_spent

                # close browser
                logger.debug("[{}] Quitting driver".format(label))
                driver.quit()

        # display total time spent on saboteur run
        logger.info("[{}] Total time spent: {} / {}".format(
            label,
            str(timedelta(seconds=total_time_spent)),
            str(timedelta(seconds=self.configuration["saboteur_timeout"]))
        ))

        self.increment() # increment number of run done

    def create_threads(self, number, saboteur=False, short=False):
        """
        Create number of threads of type saboteur, short or long

        Parameters:
            number: Number of thread to create
            saboteur: Mark if thread has to be a saboteur
            short: Mark if thread has to be short
        """

        threads = []

        if short:
            nb_thread = number // self.short_by_thread
            left = number % self.short_by_thread
            waiting = self.configuration["long_timeout"] - (self.configuration["short_timeout"] * left)
            index = 0
            for _ in range(nb_thread):
                threads.append(threading.Thread(
                    target=self.honest_run,
                    args=[self.short_by_thread, self.configuration["short_timeout"], index + 1, 0]
                ))
                index += 1
            if left:
                threads.append(threading.Thread(
                    target=self.honest_run,
                    args=[left, self.configuration["short_timeout"], index + 1, waiting]
                ))
        else:
            # loop on number of thread to create
            for index in range(number):
                # if it's a saboteur create corresponding type of thread
                if saboteur:
                    threads.append(threading.Thread(target=self.saboteur_run, args=[index + 1]))
                # create a long by default
                else:
                    threads.append(threading.Thread(
                        target=self.honest_run,
                        args=[1, self.configuration["long_timeout"], index + 1, 0]
                    ))
        # return all created threads
        return threads

def manual_run(url, token, configuration):
    """
    Debug purpose function to run manually a batch without server order

    Parameters:
        token: Connection token
        configuration: YAML configuration loaded
    """

    try:
        client = ComputerClient(args.url, token, configuration)
        # enter manually number of worker for each type
        client.start_threads({
            'type': 'START_CLIENT',
            'nb_short': int(input("\033[33;1mShort\033[0m: \033[33m")),
            'nb_long': int(input("\033[33;1mLong\033[0m: \033[33m")),
            'nb_saboteur': int(input("\033[33;1mSaboteur\033[0m: \033[33m")),
        })
        client.wait_threads()
    except ValueError:
        logger.error("Please enter only integers")

def read_configuration(path):
    """
    Utility function to parse YAML configuration file

    Parameters:
        path: Path to configuration file
    """

    with open(path, "r") as file:
        logger.debug("\033[92mReading configuration file\033[0m")
        conf = yaml.load(file, Loader=yaml.FullLoader)
    logger.debug("\033[92mLoaded configuration\033[0m")
    return conf

if __name__ == "__main__":
    # CLI configuration
    parser = argparse.ArgumentParser()
    parser.add_argument("-c", "--configuration", help="Optionnal .yaml configuration file", default="./configuration.yaml")
    parser.add_argument("-u", "--url", help="Optional Websocket server URL to connect to", default="wss://provider.bot-jarvis.fr/coordinator/")
    parser.add_argument("-i", "--interactive", action="store_true", help="Manual batch run")
    parser.add_argument("-v", "--verbose", action="store_true", help="Verbose output")
    args = parser.parse_args()

    token = os.environ.get("TOKEN")

    # verbose configuration
    if args.verbose:
        logger.setLevel(logging.DEBUG)
    else:
        logger.setLevel(logging.INFO)

    # check provided path exists
    if os.path.isfile(args.configuration) and token:
        # parse configuration
        configuration = read_configuration(args.configuration)
        
        # interactive configuration
        if args.interactive:
            manual_run(args.url, token, configuration)
        else:
            ComputerClient(args.url, token, configuration).run()
    else:
        logger.error("Provided configuration file doesn't exist: {} or TOKEN is empty".format(args.configuration))
